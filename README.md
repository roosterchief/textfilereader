# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository solves simple exam task: _"Write a program (and prove that it works) that: Given a text file, count the occurrence of each unique word in the file."_

### How do I get set up? ###

* Code can be compiled with VS2015+
* To start the MSTest unit tests you can use either VS or any other compatible program
* To run the program find the compiled _TextFileReader.exe_ and start it from the CMD line by adding as a parameter a path to plain text file.


### 3rd Party ###

* The file signature of the files is verified by a third party software with MIT license.

### Algorithm & Workflow of the program

1. Read the input file parameter
2. Create a service object and handle the file
3. The serivce does several things: loading the file, checking the file type (only text files supported), reading the file as stream, parsing the file and saving the results in dictionary object. 
4. The final results are printed in the CMD console

### Diagrams ###

Class Diagram of the service classes: 
![alt text](https://cdn.pbrd.co/images/H3ueybR.png "Class Diagram")

Complete Class Diagram for all interfaces and classes from the project _TextFileReaderService_: 
![alt text](https://cdn.pbrd.co/images/H3ufbuO.png "Complete Class Diagram")
