﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileReaderService
{
    internal interface IWordsParser
    {
        IEnumerable<string> Extract(string line);
    }
}
