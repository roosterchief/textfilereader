﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileReaderService
{
    /// <summary>
    /// Class for exposing the word stats functionality as a service 
    /// i.e. this is the entry point of the clients.
    /// </summary>
    public class WordStatsService : IWordStatsService
    {
        private readonly IWordsRepository _repository;
        private readonly IDictionaryPrinter _printer;

        public WordStatsService()
        {
            // NB!
            // We could use IoC container which wil give us better control over the dependencies.
            // Currently we are initializing directly 'WordsRepository' and 'DictionaryPrinter', so to speak we are binded to specific implementations.

            _repository = new WordsRepository();
            _printer = new DictionaryPrinter();
        }

        public void HandleTextFile(string fileName)
        {
            var wordStats = _repository.GetWordStatsByFileName(fileName);
            _printer.Print(wordStats);
        }
    }
}
