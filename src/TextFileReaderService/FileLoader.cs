﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileSignatures;

namespace TextFileReaderService
{
    /// <summary>
    /// Class specialized in reading of text files.
    /// </summary>
    internal class FileLoader : IFileLoader
    {
        public StreamReader Load(string fileName)
        {
            FileFormat format = GetFileFormat(fileName);
            //Encoding encoding = GetFileEncoding(fileName); // Maybe we will need the encoding at some point...

            if (format != null)
            {
                Console.WriteLine("The submitted file is not a plain text file:");
                Console.WriteLine("Media Type : " + format.MediaType);
                Console.WriteLine("Signature  : " + BitConverter.ToString(format.Signature.ToArray()));
                Console.WriteLine("Extension  : " + format.Extension);
                Console.WriteLine("This program does support anything more than plain text file!");

                return StreamReader.Null;
            }

            StreamReader streamReader = new StreamReader(fileName);
            return streamReader;
        }

        //public static Encoding GetFileEncoding(string fileName)
        //{
        //    //https://www.nuget.org/packages/SimpleHelpers.FileEncoding
        //    //MIT License
        //    var encoding = FileEncoding.DetectFileEncoding(fileName);
        //    return encoding;
        //}

        public static FileFormat GetFileFormat(string fileName)
        {
            //https://github.com/neilharvey/FileSignatures
            //MIT License
            var inspector = new FileFormatInspector();
            using (FileStream stream = File.Open(fileName, FileMode.Open))
            {
                if (stream.Length > 0)
                {
                    var format = inspector.DetermineFileFormat(stream);
                    return format;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
