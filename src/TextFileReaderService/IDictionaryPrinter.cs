﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileReaderService
{
    internal interface IDictionaryPrinter
    {
        void Print(Dictionary<string, int> wordStats);
    }
}
