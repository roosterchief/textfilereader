﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileReaderService
{
    public interface IWordStatsService
    {
        void HandleTextFile(string fileName);
    }
}
