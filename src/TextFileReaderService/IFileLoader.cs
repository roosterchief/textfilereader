﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileReaderService
{
    internal interface IFileLoader
    {
        StreamReader Load(string fileName);
    }
}
