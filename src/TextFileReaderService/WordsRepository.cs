﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileReaderService
{
    /// <summary>
    /// Class specialized in doing words operations.
    /// </summary>
    internal class WordsRepository : IWordsRepository
    {
        private readonly IFileLoader _loader;
        private readonly IWordsParser _parser;
        private readonly Dictionary<string, int> _wordStats = new Dictionary<string, int>();

        public WordsRepository()  
        {
            // NB!
            // We could use IoC container which wil give us better control over the dependencies.
            // Currently we are initializing directly 'FileLoader' and 'WordsParser', so to speak we are binded to specific implementations.

            _loader = new FileLoader();
            _parser = new WordsParser();
        }

        public Dictionary<string, int> GetWordStatsByFileName(string fileName)
        {
            ResetWordStats();

            using (StreamReader sr = _loader.Load(fileName))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    IEnumerable<string> words = _parser.Extract(line);
                    UpdateWordStats(words);
                }
            }

            return _wordStats;
        }

        private void UpdateWordStats(IEnumerable<string> words)
        {
            foreach (string word in words)
            {
                if (_wordStats.ContainsKey(word))
                {
                    _wordStats[word] = ++_wordStats[word];
                }
                else
                {
                    _wordStats.Add(word, 1);
                }
            }
        }

        private void ResetWordStats()
        {
            _wordStats.Clear();
        }
    }
}
