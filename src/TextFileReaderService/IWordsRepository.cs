﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileReaderService
{
    internal interface IWordsRepository
    {
        Dictionary<string, int> GetWordStatsByFileName(string fileName);
    }
}
