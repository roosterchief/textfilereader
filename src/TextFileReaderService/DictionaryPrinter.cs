﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileReaderService
{
    /// <summary>
    /// Class specialized in printing of dictionary objects
    /// </summary>
    internal class DictionaryPrinter : IDictionaryPrinter
    {
        public void Print(Dictionary<string, int> wordStats)
        {
            foreach (KeyValuePair<string, int> keyValuePair in wordStats)
            {
                Console.WriteLine($"{keyValuePair.Value}:{keyValuePair.Key}");
            }
        }
    }
}
