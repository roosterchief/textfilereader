﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileReaderService
{
    /// <summary>
    /// Class specialized in extracting of words.
    /// </summary>
    internal class WordsParser : IWordsParser
    {
        /// <summary>
        /// Cracks a given string to separate words and returns collection of words
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public IEnumerable<string> Extract(string line)
        {
            char[] punctuation = line.Where(Char.IsPunctuation).Distinct().ToArray(); // get an array of all punctuation symbols
            IEnumerable<string> words = line.Split().Select(x => x.Trim(punctuation)); // first trims the punctuation signs and then splits a line based on the punctuation

            return words;
        }
    }
}
