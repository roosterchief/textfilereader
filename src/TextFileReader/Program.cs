﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TextFileReader
{
    /// <summary>
    /// This is my original program, delivered as response of the first reqiest to create the exercise.
    /// </summary>
    class Program
    {
        private static readonly Dictionary<string, int> wordStat = new Dictionary<string, int>();

        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Missing file path argument! Start the program with specified file path.");
                return;
            }

            string filepath = args[0];

            if (!File.Exists(filepath))
            {
                Console.WriteLine($"The specified file '{filepath}' does not exist! MaKe sure that the file path is correct.");
                return;
            }

            try
            {

                using (StreamReader sr = File.OpenText(filepath))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        char[] punctuation = line.Where(Char.IsPunctuation).Distinct().ToArray();
                        IEnumerable<string> words = line.Split().Select(x => x.Trim(punctuation));

                        foreach (string word in words)
                        {
                            if (wordStat.ContainsKey(word))
                            {
                                wordStat[word] = ++wordStat[word];
                            }
                            else
                            {
                                wordStat.Add(word, 1);
                            }
                        }
                    }
                }

                foreach (KeyValuePair<string, int> keyValuePair in wordStat)
                {
                    Console.WriteLine($"{keyValuePair.Value}:{keyValuePair.Key}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong! Check exception meessage for details.");
                Console.WriteLine(ex.Message);
            }
        }
    }
}
