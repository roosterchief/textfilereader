﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextFileReaderService;

namespace TextFileReader
{
    /// <summary>
    /// Improved program by segragating the main responsabilities in different classes.
    /// For this concrete example the improved design is not benefitial but it shows what could be made in complex scenarios.
    /// </summary>
    /// <param name="args"></param>
    class Program2
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Missing file path argument! Start the program with specified file path.");
                return;
            }

            if (args.Length > 1)
            {
                Console.WriteLine("Multipple arguments detected. Start the program with one argument - the file path.");
                return;
            }

            try
            {
                // NB!
                // We could use IoC container which wil give us better control over the dependencies.
                // Currently we are initializing directly 'WordsStatsService' so to speak we are binded to specific implementations.

               IWordStatsService wss = new WordStatsService();
               wss.HandleTextFile(args[0]);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong! Check exception meessage for details.");
                Console.WriteLine(ex.Message);
            }
        }
    }
}
