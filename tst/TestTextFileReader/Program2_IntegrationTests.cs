﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestTextFileReader
{
    [TestClass]
    public class Program2_IntegrationTests
    {
        private Process SetupProgram(string argumentFile)
        {
            string curDir = Directory.GetCurrentDirectory(); // E.g. C:\source\TextFileReader\tst\TestTextFileReader\bin\Debug
            string rootDir = curDir.Split(new string[] { "\\tst\\" }, StringSplitOptions.None)[0]; // E.g.  C:\source\TextFileReader\
            string dataDir = Path.Combine(rootDir, "tst\\TestData");
            string exeDir = Path.Combine(rootDir, "src\\TextFileReader\\bin\\Debug");
            
            // Start the child process.
            Process p = new Process
            {
                StartInfo =
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    FileName = Path.Combine(exeDir, "TextFileReader.exe"),
                    Arguments = !string.IsNullOrWhiteSpace(argumentFile) ? Path.Combine(dataDir, argumentFile) : ""
                }
            };

            return p;
        }

        private static string ExecuteProgram(Process p)
        {
            p.Start();
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            return output;
        }

        [TestMethod]
        public void Main_WhenOneArugumentAvailable_ProgramExecutes()
        {
            // Arrange
            var process = SetupProgram("test1.txt");
            
            // Act
            var output = ExecuteProgram(process);

            // Assert
            Assert.IsTrue(output.Contains("1:Lorem"));
        }

        [TestMethod]
        public void Main_WhenZeroArugumentsAvailable_ProgramStops()
        {
            // Arrange
            var process = SetupProgram(null);

            // Act
            var output = ExecuteProgram(process);

            // Assert
            Assert.IsTrue(output.Contains("Missing file path argument! Start the program with specified file path."));

        }

        [TestMethod]
        public void Main_WhenMoreThanOneArugumentAvailable_ProgramStops()
        {
            // Arrange
            var process = SetupProgram("first_argument second_argument");

            // Act
            var output = ExecuteProgram(process);

            // Assert
            Assert.IsTrue(output.Contains("Multipple arguments detected. Start the program with one argument - the file path."));
        }

        [TestMethod]
        public void Main_WhenBinaryFileArgument_ProgramStops()
        {
            // Arrange
            var process = SetupProgram("test3.jpg");

            // Act
            var output = ExecuteProgram(process);

            // Assert
            Assert.IsTrue(output.Contains("The submitted file is not a plain text file"));
        }

        [TestMethod]
        public void Main_WhenFileArugmentZeroLengthFile_ProgramHasNoOutput()
        {
            // Arrange
            var process = SetupProgram("test2.txt");

            // Act
            var output = ExecuteProgram(process);

            // Assert
            Assert.IsTrue(output == string.Empty);
        }
    }
}
