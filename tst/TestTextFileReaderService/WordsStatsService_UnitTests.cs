﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TextFileReaderService;

namespace TestTextFileReaderService
{
    [TestClass]
    public class WordsStatsService_UnitTests
    {
        [TestMethod]
        public void HandleTextFile_WhenParameterIsExistingTextFile_EverythingWorks()
        {
            // Arrange 
            string dataDir = TestFileHelper.FindTestDataPath();
            WordStatsService service = new WordStatsService();

            // Act 
            service.HandleTextFile(Path.Combine(dataDir, "test1.txt"));

            // Assert
            // Nothing to assert.
        }

        [ExpectedException(typeof(FileNotFoundException))]
        [TestMethod]
        public void HandleTextFile_WhenParameterIsMissingFile_ThrowsException()
        {
            // Arrange 
            string dataDir = TestFileHelper.FindTestDataPath();
            WordStatsService service = new WordStatsService();

            // Act 
            service.HandleTextFile(Path.Combine(dataDir, "missing_file"));
        }

        [TestMethod]
        public void HandleTextFile_WhenParameterIsZeroLengthFile_ThrowsException()
        {
            // Arrange 
            string dataDir = TestFileHelper.FindTestDataPath();
            WordStatsService service = new WordStatsService();

            // Act 
            service.HandleTextFile(Path.Combine(dataDir, "test2.txt"));

            // Assert
            // Nothing to assert.
        }

        [TestMethod]
        public void HandleTextFile_WhenParameterIsBinaryFile_ThrowsException()
        {
            // Arrange 
            string dataDir = TestFileHelper.FindTestDataPath();
            WordStatsService service = new WordStatsService();

            // Act 
            service.HandleTextFile(Path.Combine(dataDir, "test3.jpg"));

            // Assert
            // Nothing to assert.
        }
    }   
}
