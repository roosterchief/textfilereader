﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TextFileReaderService;

namespace TestTextFileReaderService
{
    [TestClass]
    public class DictionaryPrinter_UnitTests
    {
        [TestMethod]
        public void Print_WhenParameterHasRecords_OutputsResult()
        {
            // Arrange
            DictionaryPrinter dp = new DictionaryPrinter();
            Dictionary<string, int> data = new Dictionary<string, int> {{"Ttest", 1}};

            // Act
            dp.Print(data);

            // Assert
            // ..Nothing to assert at the moment
        }

        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void Print_WhenParameterIsNull_ThrowsException()
        {
            // Arrange
            DictionaryPrinter dp = new DictionaryPrinter();

            // Act
            dp.Print(null);

            // Assert
            // S. method attribute 'ExpectedException(typeof(NullReferenceException))'
        }
    }
}
