﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTextFileReaderService
{
    public class TestFileHelper
    {
        public static string FindTestDataPath()
        {
            string curDir = Directory.GetCurrentDirectory(); // E.g. C:\source\TextFileReader\tst\TestTextFileReaderService\bin\Debug
            string rootDir = curDir.Split(new string[] { "\\tst\\" }, StringSplitOptions.None)[0]; // E.g.  C:\source\TextFileReader\
            string dataDir = Path.Combine(rootDir, "tst\\TestData");

            return dataDir;
        }
    }
}
