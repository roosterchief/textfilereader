﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TextFileReaderService;

namespace TestTextFileReaderService
{
    [TestClass]
    public class FileLoader_UnitTests
    {
        

        [TestMethod]
        public void Load_WhenParameterIsPathToTextFile_ReturnsStream()
        {
            // Arrange
            string dataDir = TestFileHelper.FindTestDataPath();
            FileLoader fl = new FileLoader();

            // Act
            using (StreamReader stream = fl.Load(Path.Combine(dataDir, "test1.txt")))
            {

                // Assert
                Assert.IsNotNull(stream);
            }
        }

        [TestMethod]
        public void Load_WhenParameterIsZeroLengthFile_ReturnsStream()
        {
            // Arrange
            string dataDir = TestFileHelper.FindTestDataPath();
            FileLoader fl = new FileLoader();

            // Act
            using (StreamReader stream = fl.Load(Path.Combine(dataDir, "test2.txt")))
            {

                // Assert
                Assert.IsNotNull(stream);
                Assert.IsNotNull(stream.BaseStream.Length == 0);
            }
        }

        [TestMethod]
        public void Load_WhenParameterIsPathToOtherTypeFile_ReturnsStream()
        {
            // Arrange
            string dataDir = TestFileHelper.FindTestDataPath();
            FileLoader fl = new FileLoader();

            // Act
            using (StreamReader stream = fl.Load(Path.Combine(dataDir, "test3.jpg")))
            {

                // Assert
                Assert.IsTrue(stream == StreamReader.Null);
            }

        }

        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void Load_WhenParameterIsNull_ThrowsException()
        {
            // Arrange
            FileLoader fl = new FileLoader();

            // Act
            using (StreamReader stream = fl.Load(null))
            {

            }

            // Assert
            // S. method attribute 'ExpectedException(typeof(ArgumentNullException))'
        }
    }
}
