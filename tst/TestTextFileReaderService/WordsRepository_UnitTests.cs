﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TextFileReaderService;

namespace TestTextFileReaderService
{
    [TestClass]
    public class WordsRepository_UnitTests
    {
        [TestMethod]
        public void GetWordStatsByFileName_WhenParameterIsExistingTextFile_OutputsResult()
        {
            // Arrange
            string dataDir = TestFileHelper.FindTestDataPath();
            WordsRepository repo = new WordsRepository();

            // Act
            Dictionary<string, int> wordsStat = repo.GetWordStatsByFileName(Path.Combine(dataDir, "test1.txt"));

            // Assert
            Assert.IsTrue(wordsStat.Count == 81);
        }

        [TestMethod]
        public void GetWordStatsByFileName_WhenParameterIsZeroLengthFile_OutputsResult()
        {
            // Arrange
            string dataDir = TestFileHelper.FindTestDataPath();
            WordsRepository repo = new WordsRepository();

            // Act
            Dictionary<string, int> wordsStat = repo.GetWordStatsByFileName(Path.Combine(dataDir, "test2.txt"));

            // Assert
            Assert.IsTrue(wordsStat.Count == 0);
        }

        [TestMethod]
        public void GetWordStatsByFileName_WhenParameterIsBinaryFile_OutputsResult()
        {
            // Arrange
            string dataDir = TestFileHelper.FindTestDataPath();
            WordsRepository repo = new WordsRepository();

            // Act
            Dictionary<string, int> wordsStat = repo.GetWordStatsByFileName(Path.Combine(dataDir, "test2.txt"));

            // Assert
            Assert.IsTrue(wordsStat.Count == 0);
        }

        [ExpectedException(typeof(FileNotFoundException))]
        [TestMethod]
        public void GetWordStatsByFileName_WhenParameterIsMissingFile_ThrowsException()
        {
            // Arrange
            string dataDir = TestFileHelper.FindTestDataPath();
            WordsRepository repo = new WordsRepository();

            // Act
            repo.GetWordStatsByFileName(Path.Combine(dataDir, "missing_file"));

            // Assert
            // S. method attribute 'ExpectedException(typeof(FileNotFoundException))'
        }
    }
}
