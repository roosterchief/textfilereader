﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TextFileReaderService;

namespace TestTextFileReaderService
{

    [TestClass]
    public class WordsParser_UnitTests
    {

        [TestMethod]
        public void Extract_WhenParameterIsTextLine_ResultStringCollection()
        {
            // Arrange
            WordsParser wp = new WordsParser();

            // Act
            IEnumerable<string> words = wp.Extract("Lorem ipsum dolor sit amet, consectetur adipiscing elit, dolor");

            // Assert
            Assert.IsTrue(words.ToList().Count == 9);
        }

        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void Extract_WhenParameterIsNull_ThrowsException()
        {
            // Arrange
            WordsParser wp = new WordsParser();

            // Act
            wp.Extract(null);

            // Assert
            // S. method attribute 'ExpectedException(typeof(ArgumentNullException))'
        }
    }
}
